import socket
from os import listdir, getcwd
from os.path import isfile, join
from threading import Thread


clients = []


# Thread to listen one particular client
class ClientListener(Thread):
    def __init__(self, name: str, sock: socket.socket):
        super().__init__(daemon=True)
        self.sock = sock
        self.name = name

    # clean up
    def _close(self):
        clients.remove(self.sock)
        self.sock.close()
        print(self.name + ' disconnected')

    def run(self):
        # try to read 1024 bytes from user
        # this is blocking call, thread will be paused here
        data = self.sock.recv(1024).decode()
        if not data:
            # if we got no data – client has disconnected
            self._close()
            # finish the thread
            return

        # separating name of the file from extension part
        file_name = data.split('.')
        ext = file_name[1]
        file_name = file_name[0]

        # analysing copies of file and deciding the filename
        onlyfiles = [f for f in listdir(getcwd()) if isfile(join(getcwd(), f))]
        if file_name + '.' + ext in  onlyfiles:
            count = 0
            tmp_file = file_name + '_copy' + str(count) + '.' + ext
            print(tmp_file)
            while tmp_file in onlyfiles:
                count += 1
                tmp_file = file_name + '_copy' + str(count) + '.' + ext

            file_name = tmp_file
        else:
            file_name = file_name + '.' + ext

        print('File name', file_name)

        self.sock.sendall("-".encode())

        data = self.sock.recv(1024).decode(errors='ignore')
        if not data:
            # if we got no data – client has disconnected
            self._close()
            # finish the thread
            return

        file_size = int(data)
        print('File size', file_size)

        self.sock.sendall("+".encode())

        byte_file = bytearray()

        step = 1024
        prev = 0
        for i in range(step, file_size, step):
            byte_file.extend(self.sock.recv(step))
            print('\rProgress ', int((i / file_size) * 100), '%', end='')
            prev = i

        byte_file.extend(self.sock.recv(file_size-prev))
        print('\rProgress 100%')

        f = open(file_name, 'wb')
        f.write(byte_file)
        f.close()

        self.sock.sendall("*".encode())
        print('Transaction finished')

        self._close()
        # finish the thread
        return


def main():
    next_name = 1
    PORT = 8800
    # AF_INET – IPv4, SOCK_STREAM – TCP
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # reuse address; in OS address will be reserved after app closed for a while
    # so if we close and imidiatly start server again – we'll get error
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # listen to all interfaces at 8800 port
    sock.bind(('', PORT))
    sock.listen()
    while True:
        # blocking call, waiting for new client to connect
        con, addr = sock.accept()
        clients.append(con)
        name = 'u' + str(next_name)
        next_name += 1
        print(str(addr) + ' connected as ' + name)
        # start new thread to deal with client
        ClientListener(name, con).start()


if __name__ == "__main__":
    main()
