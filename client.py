import socket
import sys
import time


def send_picture():
    HOST = '35.181.43.54'  # The server's hostname or IP address
    # HOST = '127.0.0.1'
    PORT = 8800       # The port used by the server

    pic_name = str(sys.argv[1]) if len(sys.argv) > 1 else None
    if pic_name is None:
        print('Please, provide valid picture name')
    else:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))

            pic = open(pic_name, 'rb')
            pic_bytes = pic.read()
            size = len(pic_bytes)

            s.sendall(pic_name.encode())

            if s.recv(1024).decode() == "-":
                pass

            s.sendall(str(size).encode())

            if s.recv(1024).decode() == "+":
                pass

            step = 1024
            prev = 0
            for i in range(step, size, step):
                s.sendall(pic_bytes[prev:i])
                print('\rProgress ', int((i/size)*100), ' %', end='')
                time.sleep(0.1)
                prev = i

            s.sendall(pic_bytes[prev:size])
            print('\rProgress 100%')

            print('Transaction finished')

            if s.recv(1024).decode() == "*":
                pass

            s.close()


if __name__ == "__main__":
    send_picture()
